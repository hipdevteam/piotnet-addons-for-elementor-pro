<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
    
add_action( 'wp_ajax_pafe_twilio_sendgrid_get_field', 'pafe_twilio_sendgrid_get_field' );
add_action( 'wp_ajax_nopriv_pafe_twilio_sendgrid_get_field', 'pafe_twilio_sendgrid_get_field' );

function pafe_twilio_sendgrid_get_field(){
	$api_key = $_REQUEST['api_key'];
	if (!empty($api_key)) {
        $url = 'https://api.sendgrid.com/v3/marketing/field_definitions';
        $args = [
            'method' => 'GET',
            'sslverify' => false,
            'headers' => [
                'Authorization' => 'Bearer ' . $api_key,
                'Content-Type' => 'application/json'
            ],
        ];
        $response = wp_remote_request($url, $args);
        $res_data = json_decode(wp_remote_retrieve_body($response));
        $custom_fields = $res_data->custom_fields;
        $reserved_fields = $res_data->reserved_fields;
        $html = '<div class="pafe-sendgrid-fields">';
        if(!empty($reserved_fields)){
            $html .= '<hr><div class="pafe-sendgrid-reserved-field"><div class="pafe-sendgrid-reserved-field__title">Reserved Fields</div>';
            foreach($reserved_fields as $field){
                $html .= '<div class="pafe-sendgrid-reserved-field__item"><label class="pafe-sendgrid-reserved-field__label">'.ucfirst(str_replace('_', ' ', strtolower($field->name))).'</label><div class="pafe-sendgrid-reserved-field__name"><input type="text" value="'.$field->name.'" readonly></div></div>';
            }
            $html .= '</div>';
        }
        if(!empty($custom_fields)){
            $html .= '<hr><div class="pafe-sendgrid-reserved-field"><div class="pafe-sendgrid-reserved-field__title">Custom Fields</div>';
            foreach($custom_fields as $field){
                $html .= '<div class="pafe-sendgrid-reserved-field__item"><label class="pafe-sendgrid-reserved-field__label">'.ucfirst(str_replace('_', ' ', strtolower($field->name))).'</label><div class="pafe-sendgrid-reserved-field__name"><input type="text" value="'.$field->name.'" readonly></div></div>';
            }
            $html .= '</div>';
        }
        $html .= '</div>';
        echo $html;
		wp_die();
	} else {
		echo "Please enter the API key.";
		wp_die();
	}
}
